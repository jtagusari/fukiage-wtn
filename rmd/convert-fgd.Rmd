---
title: "Convert FGD"
author: "Junta Tagusari"
output: 
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
    number_sections: true
editor_options: 
  chunk_output_type: console
---


# なにがしたいか
 - 基盤地図情報を解凍してgpkgに変換する


# 設定

## ライブラリ
```{r loadlib, message = F}
library(tidyverse)
library(sf)
```

## セッション情報
```{r sessioninfo}
sessionInfo()
Sys.time()
```

# パス

```{r}
dir_zip = "fgd/zip"
dir_gml = "fgd/gml"
path_gpkg = "fgd/converted.gpkg"
```


# zip解凍

```{r eval = F}
df_path <- tibble(
  mesh = list.files(dir_zip, full.names = F, pattern = "*.zip") %>%
    str_extract("[0-9]{6}"),
  path_zip = list.files(dir_zip, full.names = T, pattern = "*.zip"),
  path_gml = purrr::map(path_zip, ~unzip(.x, exdir = dir_gml))
)

save(df_path, file = "fgd/fgd-zip-path.RData")
```

```{r}
load("fgd/fgd-zip-path.RData")

df_path
```

# GML読込

## ファイルパス
```{r}
df_path_gml <- tibble(
  name = list.files(dir_gml, full.names = F) %>% 
    str_subset("FG-GML-"),
  mesh = str_remove(name, "FG-GML-") %>% str_extract("[0-9]{6}"),
  type = str_remove(name, "FG-GML-[[:digit:]]{6}-") %>% str_extract("[a-zA-Z]*")
) %>% 
  tidyr::nest(gml_file = c(mesh, name)) %>% 
  dplyr::mutate(n_file = purrr::map_int(gml_file,~nrow(.x)))

df_path_gml
```

## 読込

```{r}
df_feature <- df_path_gml %>% 
  dplyr::mutate(
    features = purrr::map(
      gml_file,
      ~purrr::map_dfr(
        paste0(dir_gml,"/", .x$name),
        ~st_read(.x, quiet=T) %>% 
          dplyr::rename_with(.cols = matches("fid"), .fn = ~paste0("fgd_",.x))
      )
    ),
    n_features = purrr::map_int(features, ~nrow(.x))
  )

df_feature
```

## 保存

```{r eval = F}
purrr::map2(
  df_feature$type, df_feature$features,
  ~st_write(.y, path_gpkg, layer = .x, overwrite = T, append = F)
)
```
