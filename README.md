# 吹上浜沖の洋上風力発電による健康リスクの推定について

## 概要

2022/10/16に発表した資料や関連するファイルです。

発表資料は`ppt`，計算結果は`hrisk-results`にそれぞれ格納しています。

計算は，H-RISK( https://gitlab.com/jtagusari/hrisk-wtn )で行っています。
