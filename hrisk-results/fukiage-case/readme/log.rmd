# 実行ログ

`r linkRelFile(GENERAL$PATH$LOG)`参照。

```{r echo = F}
log_txt <- read_file(GENERAL$PATH$LOG) %>%
  str_replace_all("\r\n","<br>") %>% 
  str_replace_all("\\\\","/")
```

<div style="width: 100%; height: 300px; overflow-y: scroll; border: 1px #999999 solid;">
`r log_txt`
</div> 