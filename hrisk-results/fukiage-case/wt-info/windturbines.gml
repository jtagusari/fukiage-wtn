<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ windturbines.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>130.1408220940073</gml:X><gml:Y>31.45555446520987</gml:Y><gml:Z>100</gml:Z></gml:coord>
      <gml:coord><gml:X>130.2788248005808</gml:X><gml:Y>31.72010822117727</gml:Y><gml:Z>100</gml:Z></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                      
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.0">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.149894316514,31.4556971941215,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.1">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.154471154389,31.4556738147941,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.2">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.159047986929,31.4556502719361,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.3">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.163624814095,31.4556265655479,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.4">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.168201635851,31.4556026956304,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.5">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.17277845216,31.4555786621841,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.6">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.177355262984,31.4555544652099,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.7">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.181254860595,31.456877384597,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.8">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.184377743715,31.4597457215266,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.9">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.18750081745,31.4626139782888,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.10">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.190624081829,31.4654821548578,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.11">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.193747536881,31.4683502512083,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.12">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.196573441149,31.471421668212,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.13">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.199138824427,31.4746711215836,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.14">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.202581009735,31.477158565898,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.15">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.206183532057,31.4795479963007,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.16">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.209420294434,31.4823226534605,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.17">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.212657247875,31.4850972247401,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.18">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.215894392406,31.4878717101129,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.19">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.219106155373,31.4906674613978,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.20">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.222258795378,31.4935126535585,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.225411626472,31.4963577639138,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.228564648681,31.4992027924379,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.231717862036,31.5020477391049,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.234871266562,31.5048926038891,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23802486229,31.5077373867647,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.241178649247,31.5105820877057,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.244332627461,31.5134267066864,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.247472711202,31.5162823398407,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.250517691082,31.519212963053,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.253562861493,31.5221435094679,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.256608222464,31.5250739790607,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.259653774028,31.5280043718064,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.262699516213,31.5309346876802,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.265100557223,31.5342408260639,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.267165483017,31.5377429319238,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.269230565124,31.541245000068,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.271295803582,31.5447470304827,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.273361198427,31.5482490231543,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.275426749697,31.5517509780689,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.277492457427,31.5552528952129,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278235178387,31.5590306259819,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27828048936,31.5629537524529,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278325807758,31.5668768764581,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278371133583,31.5707999979975,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278416466835,31.5747231170707,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278461807516,31.5786462336777,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278507155627,31.5825693478183,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278552511171,31.5864924594922,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278597874147,31.5904155686993,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278643244559,31.5943386754395,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278688622407,31.5982617797126,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278734007692,31.6021848815183,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278779400416,31.6061079808565,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.278824800581,31.6100310777271,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.277792467977,31.6138234660794,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.276475003766,31.6175812724873,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.275157436689,31.6213390648154,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.273839766717,31.6250968430576,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.272521993823,31.6288546072078,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.271204117978,31.6326123572598,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.269448612776,31.6361922129385,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.266875622397,31.6394397362507,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.264302454473,31.6426872089019,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.261729108967,31.6459346308723,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.259155585842,31.649182002142,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.256493906951,31.6523753791976,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.253725412073,31.6555033178617,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.250956732658,31.6586311978366,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.248187868673,31.6617590191001,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24541882008,31.6648867816302,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.242649586847,31.6680144854048,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.239671228865,31.6709773465518,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.236249988169,31.6735909915026,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23282855542,31.6762045465569,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.229412728134,31.6788228358223,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.226451216876,31.6818192420429,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.223489515912,31.6848155809627,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.220527625207,31.6878118525578,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.217565544729,31.6908080568038,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.214268827942,31.6934968291217,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.210560250168,31.6958071668764,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.206851487568,31.6981173986463,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.203912947737,31.7011069918574,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.201096867156,31.7042046892524,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.198265824851,31.7072919032567,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.86">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.195243072088,31.7102439533785,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.87">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.192220128211,31.713195933222,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.88">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.189196993189,31.7161478427624,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.89">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.18617366699,31.719099681975,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.90">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.182129479085,31.7201082211773,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.91">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.177539770006,31.7200780709047,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.92">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.172950060745,31.7200477553411,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.93">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.168360351341,31.7200172744867,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.94">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.16377064183,31.7199866283419,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.95">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.159180932252,31.719955816907,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.96">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.154591222642,31.7199248401826,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.97">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.15000151304,31.7198936981688,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.98">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.145411803482,31.7198623908662,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.99">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.140822094007,31.719830918275,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
</ogr:FeatureCollection>
