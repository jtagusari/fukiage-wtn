<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ windturbines.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>130.14082209</gml:X><gml:Y>31.45555447</gml:Y><gml:Z>100</gml:Z></gml:coord>
      <gml:coord><gml:X>130.2788248</gml:X><gml:Y>31.72317068</gml:Y><gml:Z>100</gml:Z></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                             
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.0">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.14989432,31.45569719,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.1">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.15447115,31.45567381,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.2">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.15904799,31.45565027,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.3">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.16362481,31.45562657,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.4">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.16820164,31.4556027,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.5">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.17277845,31.45557866,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.6">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.17735526,31.45555447,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.7">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.18125486,31.45687738,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.8">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.18437774,31.45974572,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.9">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.18750082,31.46261398,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.10">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.19062408,31.46548215,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.11">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.19374754,31.46835025,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.12">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.19657344,31.47142167,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.13">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.19913882,31.47467112,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.14">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.20258101,31.47715857,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.15">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.20618353,31.479548,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.16">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.20942029,31.48232265,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.17">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21265725,31.48509722,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.18">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21589439,31.48787171,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.19">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21910616,31.49066746,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.20">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2222588,31.49351265,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22541163,31.49635776,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22856465,31.49920279,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23171786,31.50204774,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23487127,31.5048926,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23802486,31.50773739,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24117865,31.51058209,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24433263,31.51342671,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24747271,31.51628234,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25051769,31.51921296,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25356286,31.52214351,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25660822,31.52507398,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25965377,31.52800437,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26269952,31.53093469,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26510056,31.53424083,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26716548,31.53774293,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26923057,31.541245,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2712958,31.54474703,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2733612,31.54824902,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27542675,31.55175098,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27749246,31.5552529,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27823518,31.55903063,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27828049,31.56295375,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27832581,31.56687688,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27837113,31.5708,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27841647,31.57472312,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27846181,31.57864623,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27850716,31.58256935,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27855251,31.58649246,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27859787,31.59041557,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27864324,31.59433868,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27868862,31.59826178,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27873401,31.60218488,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2787794,31.60610798,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2788248,31.61003108,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27779247,31.61382347,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.276475,31.61758127,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27515744,31.62133906,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27383977,31.62509684,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27252199,31.62885461,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27120412,31.63261236,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26944861,31.63619221,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26687562,31.63943974,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26430245,31.64268721,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26172911,31.64593463,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25915559,31.649182,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25649391,31.65237538,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25372541,31.65550332,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25095673,31.6586312,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24818787,31.66175902,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24541882,31.66488678,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24264959,31.66801449,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23967123,31.67097735,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23624999,31.67359099,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23282856,31.67620455,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22941273,31.67882284,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22645122,31.68181924,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22348952,31.68481558,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22052763,31.68781185,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21756554,31.69080806,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21426883,31.69349683,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21056025,31.69580717,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.20685149,31.6981174,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.20391295,31.70110699,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.20109687,31.70420469,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.19826582,31.7072919,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.86">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.19524307,31.71024395,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.87">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.19222013,31.71319593,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.88">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.18919699,31.71614784,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.89">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.18617367,31.71909968,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.90">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.18212948,31.72010822,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.91">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.17753977,31.72007807,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.92">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.17295006,31.72004776,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.93">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.16836035,31.72001727,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.94">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.16377064,31.71998663,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.95">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.15918093,31.71995582,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.96">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.15459122,31.71992484,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.97">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.15000151,31.7198937,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.98">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.1454118,31.71986239,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.99">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.14082209,31.71983092,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.100">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27659547,31.64929955,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.101">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2752366,31.65326333,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.102">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27387761,31.65722709,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.103">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27251851,31.66119083,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.104">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27144377,31.6652146,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.105">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27049097,31.66926412,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.106">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26953809,31.67331363,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.107">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26715,31.67668355,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.108">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26364216,31.67952325,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.109">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25979698,31.6818936,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.110">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25524918,31.68328603,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.111">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25070124,31.68467829,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.112">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24615317,31.6860704,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.113">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24196177,31.68793392,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.114">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23861282,31.69091096,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.115">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23526366,31.69388791,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.116">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2322271,31.69705782,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.117">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22997758,31.70071346,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.118">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22772789,31.70436906,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.119">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2270589,31.70831793,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.120">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22626341,31.71204003,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.121">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22252483,31.7146571,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.122">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21886956,31.71735313,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.123">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21544299,31.72026581,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.124">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21200749,31.72317068,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.125">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.27136298,31.64630955,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.126">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26924256,31.65002069,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.127">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26712198,31.65373179,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.128">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26500124,31.65744286,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.129">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26328551,31.66128441,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.130">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.26201732,31.66527016,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.131">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25978555,31.66874571,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.132">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25617923,31.67149343,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.133">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.25244677,31.67409467,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.134">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24830592,31.67622127,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.135">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24416487,31.67834774,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.136">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.24002364,31.68047407,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.137">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23633931,31.68313597,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.138">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.23270764,31.68585973,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.139">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22909729,31.68859763,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.140">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22692662,31.69228787,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.141">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22450659,31.69584126,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.142">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.22148496,31.69906438,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.143">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21914005,31.70259834,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.144">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21766496,31.70653184,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.145">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21504089,31.70998477,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.146">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.21207457,31.71321536,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.147">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.20847313,31.71596943,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.148">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.20490891,31.71875837,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.149">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>130.2013806,31.72158096,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
</ogr:FeatureCollection>
